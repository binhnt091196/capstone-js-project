const BASE_URL = "https://6271e186c455a64564b8f028.mockapi.io/phone";

const productService = {
  getList: () => {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  },
  postProduct: (newProduct) => {
    return axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: newProduct,
    });
  },
  delProduct: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  getProduct: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  putProduct: (id, updatedProduct) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: updatedProduct,
    });
  },
};
