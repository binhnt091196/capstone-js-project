let phoneList = [];

// lấy danh sách sản phẩm
const fetchProducts = () => {
  productService
    .getList()
    .then((res) => {
      //   console.log("res: ", res);
      phoneList = res.data;
      displayProducts(phoneList);

      //   lọc sản phẩm theo hãng
      let iphoneList = phoneList.filter((product) => product.type == 0);
      let samsungList = phoneList.filter((product) => product.type == 1);

      filterType = () => {
        let productType = document.getElementById("locTheoHang").value;
        return productType == "1"
          ? displayProducts(iphoneList)
          : productType == "2"
          ? displayProducts(samsungList)
          : displayProducts(phoneList);
      };
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

fetchProducts();

// hiển thị sản phẩm ra màn hình
const displayProducts = (list) => {
  let contentHTML = "";

  for (let product of list) {
    contentHTML += /*html*/ `
    <div class="col-3 my-3 px-1">
        <div class="card pt-5 pb-2">
            <div class="card-header text-center">
                <img class="product__img" src=${product.img} alt=${
      product.name
    } />
            </div>
            <div class="card-body mt-3">
                <h4 class="card-title">${product.name}</h4>
                <div class="d-flex justify-content-between mt-3">
                    <p class="card-text">${parseInt(
                      product.price
                    ).toLocaleString()} VND</p>
                    <button onclick="addToCart(${
                      product.id
                    })" class="btn btn-warning">Mua ngay</button>
                    </div>
            </div>
        </div>
    </div>
    `;
  }
  document.getElementById("sanPham").innerHTML = contentHTML;
};

let cart = [];

// hiển thị giỏ hàng
const renderCart = (list) => {
  let contentHTML = "";

  for (product of list) {
    contentHTML += /*html*/ `<tr>
    
    <td>
    <img 
    src=${product.img} alt=${product.name} style="width:100px;height:100px">
    </td>
    <td>${product.name}</td>
    <td>${parseInt(product.price).toLocaleString()}</td>
    <td>

    <button 
    class="btn btn-sm btn-light rounded-circle"
    onclick="editAmount(${product.id},1)"
    >
    <i class="fa fa-plus"></i>
    </button>
    <span class="mx-2">${product.amount}</span>
    <button 
    class="btn btn-sm btn-light rounded-circle"
    onclick="editAmount(${product.id},-1)"
    >
    <i class="fa fa-minus"></i>
    </button>
    
    </td>
    <td>${(parseInt(product.price) * product.amount).toLocaleString()}</td>
    <td>
    <button 
    class="btn btn-danger"
    onclick="removeProduct(${product.id})"
    >Xoá</button>
    </td>

    </tr>
      `;
  }
  calTotal();
  document.getElementById("sanPhamGioHang").innerHTML = contentHTML;
};

// thêm sản phẩm vào giỏ hàng
const addToCart = (idSanPham) => {
  productService
    .getList()
    .then((res) => {
      //   console.log("res: ", res);
      // tìm vị trí sản phẩm
      let cloneCart = res.data;

      let index = cart.findIndex((sanPham) => sanPham.id == idSanPham);

      if (index == -1) {
        let newItem = { ...cloneCart[idSanPham], amount: 1 };
        cart.push(newItem);
      } else {
        cart[index].amount++;
      }

      console.log("gioHang:", cart);
      calTotal();
      saveOnLocalStorage();
      renderCart(cart);
      return { ...cart };
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

// tăng giảm số lượng sản phẩm
const editAmount = (idSanPham, value) => {
  let index = cart.findIndex((sanPham) => sanPham.id == idSanPham);

  if (index !== 1) {
    cart[index].amount += value;
  }

  cart[index].amount == 0 && cart.splice(index, 1);
  calTotal();
  saveOnLocalStorage();
  renderCart(cart);
};

// xoá sản phẩm khỏi giỏ hàng
const removeProduct = (idSanPham) => {
  let index = cart.findIndex((sanPham) => sanPham.id == idSanPham);

  if (index !== 1) {
    cart.splice(index, 1);
    calTotal();
    saveOnLocalStorage();
    renderCart(cart);
  }
};

// tính tổng số lượng sản phẩm và tổng tiền phải trả
let totalProduct = 0;
let totalMoney = 0;
const calTotal = () => {
  totalProduct = cart.reduce((total, item) => {
    return (total += item.amount);
  }, 0);

  document.getElementById("tongSP").innerHTML = `${totalProduct}`;

  totalMoney = cart
    .reduce((total, item) => {
      return (total += item.amount * item.price);
    }, 0)
    .toLocaleString();

  document.getElementById("tongTien").innerHTML = `${totalMoney} VND`;
};

const CART_LOCALSTORAGE = "CART_LOCALSTORAGE";

// lưu trạng thái giỏ hàng vào localstorage
const saveOnLocalStorage = () => {
  let cartJson = JSON.stringify(cart);
  localStorage.setItem(CART_LOCALSTORAGE, cartJson);
};

// lấy dữ liệu giỏ hàng từ localstorage
let cartJson = localStorage.getItem(CART_LOCALSTORAGE);

if (cartJson) {
  cart = JSON.parse(cartJson);
  renderCart(cart);
}

// reset giỏ hàng
const resetCart = () => {
  cart = [];
  renderCart(cart);
  saveOnLocalStorage();
};

const purchase = () => {
  resetCart();
  alert("Thanh toán thành công!!!");
};
