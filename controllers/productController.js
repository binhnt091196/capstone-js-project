// lấy thông tin từ form
const getProductInfo = () => {
  let name = document.getElementById("tenSP").value;
  let price = document.getElementById("giaBan").value;
  let screen = document.getElementById("manHinh").value;
  let backCamera = document.getElementById("cameraSau").value;
  let frontCamera = document.getElementById("cameraTruoc").value;
  let img = document.getElementById("hinhAnh").value;
  let desc = document.getElementById("moTa").value;
  let type = document.getElementById("loaiSP").value * 1;

  return new Product(
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
};

// hiển thị thông tin sản phẩm lên form
const showProductInfo = (data) => {
  document.getElementById("tenSP").value = data.name;
  document.getElementById("giaBan").value = data.price;
  document.getElementById("manHinh").value = data.screen;
  document.getElementById("cameraSau").value = data.backCamera;
  document.getElementById("cameraTruoc").value = data.frontCamera;
  document.getElementById("hinhAnh").value = data.img;
  document.getElementById("moTa").value = data.desc;
  document.getElementById("loaiSP").value = data.type;
};
